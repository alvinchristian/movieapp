import {StyleSheet, Text, View, StatusBar, Image} from 'react-native';
import React, {useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';

const Splash = () => {
  const navigation = useNavigation();
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 2000);
  });
  return (
    <View style={styles.Container}>
      <StatusBar backgroundColor={'#fff'} barStyle="dark-content" />
      <Text style={styles.AppName}>Movies App</Text>
      <View style={styles.Image}>
        <Image
          style={styles.Logo}
          source={require('../assets/Logo.png')}></Image>
      </View>
      <Text style={styles.MyName}>Alvin Christian</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  AppName: {
    fontSize: 18,
    marginTop: 80,
    fontWeight: 'bold',
    color: '#000',
    borderRadius: 25,
    paddingHorizontal: 30,
    paddingVertical: 10,
    backgroundColor: '#fff',
  },
  Image: {
    backgroundColor: '#fff',
    paddingHorizontal: 50,
    paddingVertical: 50,
    borderRadius: 250,
    alignItems: 'center',
  },
  Logo: {
    width: 180,
    height: 180,
  },
  MyName: {
    fontSize: 14,
    marginBottom: 80,
    fontWeight: 'bold',
    color: '#000',
    borderRadius: 20,
    paddingHorizontal: 25,
    paddingVertical: 10,
    backgroundColor: '#fff',
  },
});

export default Splash;
